﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteriod : MonoBehaviour {

    // Use this for initialization
    public float Speed = 5f;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Translate(Vector3.down * Speed * Time.deltaTime);
	}
}
