﻿using UnityEngine;
using System.Collections;

public class BackGournd : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        MeshRenderer mr = GetComponent<MeshRenderer>();
        Material mt = mr.material;
        Vector3 offset = mt.mainTextureOffset;
        offset.y += Time.deltaTime;
        mt.mainTextureOffset = offset;

    }
}