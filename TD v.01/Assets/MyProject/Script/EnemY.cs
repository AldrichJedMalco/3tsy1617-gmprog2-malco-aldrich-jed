﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemY : MonoBehaviour {

    public enum Etype
    {
        Flying,
        Ground,
        Boss
    };
    public float Gold;
    public float Hp;
    public Etype Epick = Etype.Boss;

    public Transform Destination;
    public GameObject MyMonster;
    UnityEngine.AI.NavMeshAgent agent;
    public float speed = 4;
	// Use this for initialization
	void Start () {

        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.destination = Destination.position;
	}
	
	// Update is called once per frame
    void Update()
    {
        agent.speed = speed;
    }
}
