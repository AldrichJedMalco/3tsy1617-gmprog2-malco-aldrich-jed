﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerStats : MonoBehaviour {

    TowerDetection TowerDetect;
    public float Gold;
    public float BulletSpawnCountDown = 1;
    public float BulletSpawn = 0f;
    public GameObject Bullets2Shoot;
    public float ArrowDMG = 10;
    public float CannonDMG = 8;
    public float IceDMG = 5;
    public float FireDMG = 5;
    //Do Not Interfere with TowerLevel;
    public float TowerLevel = 1f;
    public Transform CurTransform;

    // Use this for initialization
    void Start () {
        TowerDetect = (TowerDetection)FindObjectOfType(typeof(TowerDetection));
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(TowerDetect.Target);
        if (TowerDetect.Target != null)
        {
            if (BulletSpawn <= 0)
            {
               
                Shoot();
                Debug.Log("Shooting");
                BulletSpawn = BulletSpawnCountDown;
            }
        }
        BulletSpawn -= Time.deltaTime;
        if (BulletSpawn < 0)
        {
            BulletSpawn = 0;
        }
       
	}
    void Shoot()
    {
        GameObject projectile = (GameObject)Instantiate(Bullets2Shoot, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);

        Projectile bullet = projectile.GetComponent<Projectile>();

        bullet.GetTarget(CurTransform);

    }
    public void CdCalculation(float temp)
    {
        BulletSpawnCountDown -= temp;
    }
}
