﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDetection : MonoBehaviour {
    public Transform Target;
    public float ConstructionTime;
    EnemY Enemy;
	// Use this for initialization
	void Start () {
        //this.gameObject.GetComponent<SphereCollider>().enabled = false;
        StartCoroutine(Spawning());
    }
	
	// Update is called once per frame
	void Update () {
	}
    void OnTriggerEnter(Collider temp)
    {
        if (temp.gameObject.tag == "Enemy")
        {
            Target = temp.transform;
            GetComponentInParent<TowerStats>().CurTransform = Target;
        }
    }
    void OnTriggerExit(Collider temp)
    {
        if (temp.gameObject.tag == "Enemy")
        {
            Target = null;
        }
    }
    IEnumerator Spawning()
    {
        while (true)
        {
            yield return new WaitForSeconds(ConstructionTime);
            Debug.Log("Construction Has Finished");
            this.gameObject.GetComponent<SphereCollider>().enabled = true;
        }

    }
}
