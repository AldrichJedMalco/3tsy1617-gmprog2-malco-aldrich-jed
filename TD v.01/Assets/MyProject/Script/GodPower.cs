﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodPower : MonoBehaviour {
    RaycastHit Hit;
    Ray ray;
    EnemySpawn EnemeySp;
    public GameObject Meteor;
    public bool Ready2meteor;
    public float MeteorCD = 5f;
    public float MeteorTimer = 0f;
    public float ApocalypseCD = 5f;
    public float ApocalypseTimer = 0f;
    // Use this for initialization
    void Start () {
        EnemeySp = (EnemySpawn)FindObjectOfType(typeof(EnemySpawn));
    }
	
	// Update is called once per frame
	void Update () {


        MeteorTimer -= Time.deltaTime;
        ApocalypseTimer -= Time.deltaTime;

        if (MeteorTimer < 0)
        {
            MeteorTimer = 0;
        }
        if (ApocalypseTimer < 0)
        {
            ApocalypseTimer = 0;
        }


        if (Ready2meteor == false || MeteorTimer != 0)
        {
            Ready2meteor = false;
            return;
        }


        MeteorButs();
	}
    public void ReaperButs()
    {
        if (ApocalypseTimer <= 0)
        {
            foreach (GameObject C in EnemeySp.MyEnemies)
            {
                Destroy(C);
                EnemeySp.EnemyAlive--;
            }
            EnemeySp.MyEnemies.Clear();
            ApocalypseTimer = ApocalypseCD;
        }
            //ApocalypseTimer = ApocalypseCD;
    }
    public void MeteorButs()
    {
        
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out Hit, 100))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Instantiate(Meteor, new Vector3(Hit.point.x, Hit.point.y + 7, Hit.point.z), Quaternion.identity);
                    Debug.Log("Construction Has Started");
                    Ready2meteor = false;
                    MeteorTimer = MeteorCD;
                }
            }
    }
    public void MeteorMakeTrue()
    {
        Ready2meteor = true;
    }
}
