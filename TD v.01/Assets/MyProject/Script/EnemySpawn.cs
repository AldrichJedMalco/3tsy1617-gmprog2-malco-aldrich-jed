﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    public GameObject[] Enemy;
    public GameObject Boss;
    private int ChosenEnemy;
    private GameObject EnemyToSpawn;
    public bool SpawningT = true;


    public List<GameObject> MyEnemies = new List<GameObject>();
    public float EnemyAlive = 0f;
    public float EnemyMax = 3f;
    public float NextWaveCounter = 0;
    public float CurrentWave = 0;
    public float CountDownTillSpawn = 3f;
	// Use this for initialization
	void Start () {
        StartCoroutine(Spawning());
	}
	
	// Update is called once per frame
	void FixedUpdate () {
     
	}
    IEnumerator Spawning()
    {
        while (true)
        {
            
            yield return new WaitUntil(() => EnemyAlive <= 0);
            yield return new WaitForSeconds(CountDownTillSpawn);
            for (NextWaveCounter = 0; NextWaveCounter < EnemyMax; NextWaveCounter++)
            {
                ChosenEnemy = Random.Range(0, Enemy.Length);
                EnemyToSpawn = Enemy[ChosenEnemy];
                GameObject arrow = (GameObject)Instantiate(EnemyToSpawn, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);
                EnemyAlive++;
                MyEnemies.Add(arrow.gameObject);
            }
            CurrentWave++;
            NextWaveCounter++;
            EnemyMax = NextWaveCounter;
            if (CurrentWave % 5 == 0)
            {
                GameObject BossSpawn = (GameObject)Instantiate(Boss, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);
            }
        }

    }
}
