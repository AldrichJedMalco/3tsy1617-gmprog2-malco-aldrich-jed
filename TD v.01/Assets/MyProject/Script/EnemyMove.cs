﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMove : MonoBehaviour {
   
    public Transform Destination;
    NavMeshAgent agent;
	// Use this for initialization
	void Start () {

        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.destination = Destination.position;
    }
	
	// Update is called once per frame
	void Update () {
	}
}
