﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreLife : MonoBehaviour {
    public EnemySpawn Gg;
    public float LifePoint = 20;
    public float GoldPoint = 200;
    // Use this for initialization
    void Start () {
       // Gg = (EnemySpawn)FindObjectOfType(typeof(EnemySpawn));
        Gg.GetComponent<EnemySpawn>();
    }
    void OnTriggerEnter(Collider temp)
    {
        if( temp.gameObject.tag == "Enemy")
        {
            Destroy(temp.gameObject);
            Gg.EnemyAlive--;
            LifePoint--;
        }
    }
    public void DeducGold(float value)
    {
        GoldPoint -= value;
    }
}
