﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerButtons : MonoBehaviour {

   public CoreLife CoreLif;
    public TowerBuild TowerB;
    public TowerStats TowerSts;
    Upgrade Sometin;
    EnemySpawn EnemeySp;
	// Use this for initialization
	void Start () {
        TowerB = (TowerBuild)FindObjectOfType(typeof(TowerBuild));
        TowerSts = (TowerStats)FindObjectOfType(typeof(TowerStats));
        CoreLif = (CoreLife)FindObjectOfType(typeof(CoreLife));
        Sometin = (Upgrade)FindObjectOfType(typeof(Upgrade));
        EnemeySp = (EnemySpawn)FindObjectOfType(typeof(EnemySpawn));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ArrowTower()
    {
        TowerB.ReadyToBuild = true;
        TowerB.Tower2Build = TowerB.Arrow;
        if (CoreLif.GoldPoint >= 100)
        {
            CoreLif.GetComponent<CoreLife>().DeducGold(TowerB.Arrow.GetComponent<TowerStats>().Gold);
            Debug.Log("Gold Deducted");
        }
        else
        {
            Debug.Log("You Have No Money Left");
            TowerB.ReadyToBuild = false;
            return;
        }
    }
    public void CannonTower()
    {
        TowerB.ReadyToBuild = true;
        TowerB.Tower2Build = TowerB.Cannon;
        if (CoreLif.GoldPoint >= 200)
        {
            CoreLif.GetComponent<CoreLife>().DeducGold(TowerB.Cannon.GetComponent<TowerStats>().Gold);
            Debug.Log("Gold Deducted");
        }
        else
        {
            Debug.Log("You Have No Money Left");
            TowerB.ReadyToBuild = false;
            return;
        }
    }
    public void FireTower()
    {
        TowerB.ReadyToBuild = true;
        TowerB.Tower2Build = TowerB.Fire;
        if (CoreLif.GoldPoint >= 500)
        {
            CoreLif.GetComponent<CoreLife>().DeducGold(TowerB.Fire.GetComponent<TowerStats>().Gold);
            Debug.Log("Gold Deducted");
        }
        else
        {
            Debug.Log("You Have No Money Left");
            TowerB.ReadyToBuild = false;
            return;
        }
    }
    public void IceTower()
    {
        TowerB.ReadyToBuild = true;
        TowerB.Tower2Build = TowerB.Ice;
        if (CoreLif.GoldPoint >= 500)
        {
            CoreLif.GetComponent<CoreLife>().DeducGold(TowerB.Ice.GetComponent<TowerStats>().Gold);
            Debug.Log("Gold Deducted");
        }
        else
        {
            Debug.Log("You Have No Money Left");
            TowerB.ReadyToBuild = false;
            return;
        }
    }
    public void UpgradeTower()
    {
        Sometin.Upgradse = true;
        Debug.Log("Button Clicked");
    }
   

}
