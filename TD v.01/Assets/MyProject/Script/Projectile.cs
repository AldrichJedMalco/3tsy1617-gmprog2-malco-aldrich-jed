﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    TowerStats TowerSts;
    TowerDetection TowerDetect;
    EnemySpawn EnemySpawn;
    EnemY Enemy;
    CoreLife CoreLif;
    public Transform CurTransform;
    //public float Damage;
    public float Speed;
    // Use this for initialization
    void Start() {
        TowerSts = (TowerStats)FindObjectOfType(typeof(TowerStats));
        TowerDetect = (TowerDetection)FindObjectOfType(typeof(TowerDetection));
        EnemySpawn = (EnemySpawn)FindObjectOfType(typeof(EnemySpawn));
        Enemy = (EnemY)FindObjectOfType(typeof(EnemY));
        CoreLif = (CoreLife)FindObjectOfType(typeof(CoreLife));
        //CurTransform = TowerSts.CurTransform;

    }

    // Update is called once per frame
    void Update() {
        if (CurTransform != null)
        {
            transform.LookAt(CurTransform.position);
            this.transform.position = Vector3.MoveTowards(transform.position, CurTransform.position, Speed);

        }
            EliminateObject();
        
    }

    public void GetTarget(Transform target)
    {
        CurTransform = target;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Enemy")
        {

            Debug.Log("ObjectHhit");
            Destroy(this.gameObject);
            if (this.gameObject.tag == "IceB")
            {
                Enemy.speed /= 1.2f;
                Enemy.Hp -= TowerSts.IceDMG;
            }
            if (this.gameObject.tag == "Arrow")
            {
                Enemy.Hp -= TowerSts.ArrowDMG;
            }
            if (this.gameObject.tag == "CannonB")
            {
                Enemy.Hp -= TowerSts.CannonDMG;
            }
            if (this.gameObject.tag == "FireB")
            {
                Enemy.Hp -= TowerSts.FireDMG;
            }
            if (Enemy.Hp <= 0)
            {
                Destroy(col.gameObject);
                EnemySpawn.EnemyAlive--;
                CoreLif.GoldPoint += 50;
                EnemySpawn.MyEnemies.Remove(col.gameObject);
            }
        }
    }
    void EliminateObject()
    {
        if(CurTransform == null)
        {
            Destroy(this.gameObject);
        }

    }
}
