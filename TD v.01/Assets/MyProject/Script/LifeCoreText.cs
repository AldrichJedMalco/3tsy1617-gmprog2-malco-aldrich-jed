﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeCoreText : MonoBehaviour {

    public Text Life;
    CoreLife CoreLeef;
	// Use this for initialization
	void Start () {
        //CoreLeef.GetComponent<CoreLife>();
        CoreLeef = (CoreLife)FindObjectOfType(typeof(CoreLife));
	}
	
	// Update is called once per frame
	void Update () {
        SetLifeText();
	}
    void SetLifeText()
    {
        Life.text = "Count: " + CoreLeef.LifePoint.ToString();
    }
}
