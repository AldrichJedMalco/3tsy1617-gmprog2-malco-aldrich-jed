﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade : MonoBehaviour {
    Ray ray;
    RaycastHit Hit;
    Projectile Proj;
    public bool Upgradse;
    CoreLife CoreLif;

	// Use this for initialization
	void Start () {
        CoreLif = (CoreLife)FindObjectOfType(typeof(CoreLife));
	}
	
	// Update is called once per frame
	void Update () {

            if (Upgradse == false)
                return;
            UpgradeT();
            Debug.Log("Ready To Upgrade");
	}
    void UpgradeT()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out Hit, 100))
            {
                if(Input.GetMouseButtonDown(0))
                {
                    if (CoreLif.GoldPoint >= 50)
                    {
                        if (Hit.transform.tag == "Cross")
                        {
                            if (Hit.transform.gameObject.GetComponent<TowerStats>().TowerLevel != 3)
                            {
                                Hit.transform.gameObject.GetComponent<TowerStats>().ArrowDMG += 2;
                                Hit.transform.gameObject.GetComponent<TowerStats>().CdCalculation(.5f);
                                Hit.transform.gameObject.GetComponent<TowerStats>().TowerLevel++;
                                CoreLif.DeducGold(50);
                                Debug.Log("CrossBow Upgraded");
                            }
                            else
                            {
                                Debug.Log("Maxed Upgrade");
                                Upgradse = false;
                            }

                        }
                        if (Hit.transform.tag == "Cannon")
                        {
                            if (Hit.transform.gameObject.GetComponent<TowerStats>().TowerLevel != 3)
                            {
                                Hit.transform.gameObject.GetComponent<TowerStats>().CannonDMG += 1;
                                Hit.transform.gameObject.GetComponent<TowerStats>().CdCalculation(.5f);
                                Hit.transform.gameObject.GetComponent<TowerStats>().TowerLevel++;
                                CoreLif.DeducGold(50);
                                Debug.Log("Cannon Upgraded");
                            }
                            else
                            {
                                Debug.Log("Maxed Upgrade");
                                Upgradse = false;
                            }
                        }
                        if (Hit.transform.tag == "Ice")
                        {
                            if (Hit.transform.gameObject.GetComponent<TowerStats>().TowerLevel != 3)
                            {
                                Hit.transform.gameObject.GetComponent<TowerStats>().IceDMG += 1;
                                Hit.transform.gameObject.GetComponent<TowerStats>().CdCalculation(.5f);
                                Hit.transform.gameObject.GetComponent<TowerStats>().TowerLevel++;
                                CoreLif.DeducGold(50);
                                Debug.Log("Ice Upgraded");
                            }
                            else
                            {
                                Debug.Log("Maxed Upgrade");
                                Upgradse = false;
                            }
                        }
                        if (Hit.transform.tag == "Fire")
                        {
                            if (Hit.transform.gameObject.GetComponent<TowerStats>().TowerLevel != 3)
                            {
                                Hit.transform.gameObject.GetComponent<TowerStats>().FireDMG += 1;
                                Hit.transform.gameObject.GetComponent<TowerStats>().CdCalculation(.5f);
                                Hit.transform.gameObject.GetComponent<TowerStats>().TowerLevel++;
                                CoreLif.DeducGold(50);
                                Debug.Log("Fire Upgraded");
                            }
                            else
                            {
                                Debug.Log("Maxed Upgrade");
                                Upgradse = false;
                            }
                        }
                    }
                    else
                    {
                        Upgradse = false;
                        Debug.Log("Failed To Upgrade");
                        return;
                    }

                }
            }
    }
}
