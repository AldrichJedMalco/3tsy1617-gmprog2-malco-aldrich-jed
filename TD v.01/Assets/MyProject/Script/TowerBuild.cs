﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBuild : MonoBehaviour {
    Ray ray;
    RaycastHit Hit;
    Random rand;
    public bool Buildable = false;
    public bool ReadyToBuild = false;
    public GameObject Arrow;
    public GameObject Cannon;
    public GameObject Fire;
    public GameObject Ice;
    public GameObject TempBuild;
    public GameObject Tower2Build;
    public GameObject tower;
	// Use this for initialization
	void Start () {
        Tower2Build = null;
    }
	
	// Update is called once per frame
	void Update () {
        if (ReadyToBuild == false)
            return;
            TowerSpawn();
	}
    //Vector3 SnapToGrid(Vector3 towerObject)
    //{
    //    return new Vector3(Mathf.Round(towerObject.x),
    //                        towerObject.y,
    //                        Mathf.Round(towerObject.z));

    //}
    void TowerSpawn()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //if (Physics.Raycast(ray, out Hit, 100))
        //{
        //    Vector3 towerPos = Hit.point;
        //    //objectTower.transform.position = SnapToGrid(towerPos);
        //    TempBuild.transform.position = SnapToGrid(towerPos);
        //}

                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100, 1 << 8))
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        Instantiate(Tower2Build, hit.point, Quaternion.identity);
                        Debug.Log("Construction Has Started");
                        Tower2Build = null;
                        ReadyToBuild = false;
                    }
                }
    }
   

}
