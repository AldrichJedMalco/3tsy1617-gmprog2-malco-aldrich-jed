﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {
    public float Boundary = 10f;
    public float CameraSpeed = 3f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D) || Input.mousePosition.x >= Screen.width - Boundary)
        {
            transform.Translate(Vector3.right * CameraSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || Input.mousePosition.x <= Boundary)
        {
            transform.Translate(Vector3.left * CameraSpeed * Time.deltaTime, Space.World);
        }

        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || Input.mousePosition.y >= Screen.height - Boundary)
        {
            transform.Translate(Vector3.forward * CameraSpeed * Time.deltaTime, Space.World);
        }

        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) || Input.mousePosition.y <= Boundary)
        {
            transform.Translate(Vector3.back * CameraSpeed * Time.deltaTime, Space.World);
        }
        transform.position = new Vector3(Mathf.Clamp(transform.position.x,14, 50),12,Mathf.Clamp(transform.position.z,2, 41));

    }
}
