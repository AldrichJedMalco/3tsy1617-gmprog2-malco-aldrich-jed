﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsTextLeft : MonoBehaviour {
    public Text Gold;
    CoreLife CoreLif;
	// Use this for initialization
	void Start () {
        CoreLif = (CoreLife)FindObjectOfType(typeof(CoreLife));
	}
	
	// Update is called once per frame
	void Update () {
        SetGoldText();
	}
    void SetGoldText()
    {
        Gold.text = "Count: " + CoreLif.GoldPoint.ToString();
    }
}
