﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorBehavior : MonoBehaviour {
    EnemY Enemi;
    public float Speed = 0.5f;
    public List<GameObject> EnemiHit = new List<GameObject>();
    // Use this for initialization
    void Start () {
        Enemi = (EnemY)FindObjectOfType(typeof(EnemY));
    }
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.down * Speed * Time.deltaTime);
        Destroy(this.gameObject, 1.5f);
    }
    void OnTriggerEnter(Collider temp)
    {
        if (temp.gameObject.tag == "Enemy")
        {
            temp.GetComponent<EnemY>().Hp *= 0.85f;
        }
    }
}
