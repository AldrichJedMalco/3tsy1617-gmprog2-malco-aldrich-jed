﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    public float EnemySpawned = 0;
    public float StartingEnemy = 0;
    public float EnemyMax = 3;
    public GameObject Foodss;
    // Use this for initialization
    void Start()
    {
        for (int o = 0; o < StartingEnemy; o++)
        {
            GameObject Stat;
            Stat = (GameObject)Instantiate(Foodss, new Vector3(Random.Range(12, -12), Random.Range(12, -12), this.transform.position.z), Quaternion.identity);
        }
        EnemySpawned = StartingEnemy;
        StartCoroutine(MyMethod());
    }

    // Update is called once per frame
    IEnumerator MyMethod()
    {
        while (true)
        {
            yield return new WaitForSeconds(10);
            if (EnemySpawned < EnemyMax)
            {
                GameObject aste;
                aste = (GameObject)Instantiate(Foodss, new Vector3(Random.Range(12, -12), Random.Range(12, -12), this.transform.position.z), Quaternion.identity);
                EnemySpawned++;
            }

        }
    }
}
