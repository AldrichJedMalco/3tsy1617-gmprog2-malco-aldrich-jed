﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float Speed;
    public GameObject Players;
    public Camera Camers;
    Spawn Spawns;
    GameManager GameManagers;
    CameraFollow CFollow;
    Enemy Enemies;
    EnemySpawn EnemiSpawn;
    // Use this for initialization
    void Start () {
        Spawns = (Spawn)FindObjectOfType(typeof(Spawn));
        GameManagers = (GameManager)FindObjectOfType(typeof(GameManager));
        CFollow = (CameraFollow)FindObjectOfType(typeof(CameraFollow));
        Enemies = (Enemy)FindObjectOfType(typeof(Enemy));
        EnemiSpawn = (EnemySpawn)FindObjectOfType(typeof(EnemySpawn));
    }
	
	// Update is called once per frame
	void Update () {
        Vector2 num = Camers.ScreenToWorldPoint(Input.mousePosition);
        num.x = Mathf.Clamp(num.x, -14, 14);
        num.y = Mathf.Clamp(num.y, -14, 14);

        Players.transform.position = Vector2.Lerp(Players.transform.position, num, Speed * Time.deltaTime);
	}
    void OnCollisionEnter(Collision other)
    {
        if (other.collider.gameObject.tag == "Food")
        {
            Debug.Log("Destroyed bitch");
            Destroy(other.collider.gameObject);
            Spawns.FoodSpawned--;
            GameManagers.FoodScore++;
            CFollow.ZoomEat(0.1f);
            Players.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
            Speed = Speed * 0.95f;

        }
        if (other.collider.gameObject.tag == "Enemy")
        {
            if (Enemies.transform.localScale.magnitude < this.transform.localScale.magnitude)
            {
                Debug.Log("Destroyed bitch");
                Destroy(other.collider.gameObject);
                GameManagers.FoodScore += 3;
                CFollow.ZoomEat(0.3f);
                Players.transform.localScale += new Vector3(0.3f, 0.3f, 0.3f);
                Speed = Speed * 0.95f;
                EnemiSpawn.EnemySpawned--;
            } else Destroy(this.gameObject);

        }
    }
}
