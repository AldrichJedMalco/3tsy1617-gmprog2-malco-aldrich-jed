﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    Player Players;
    CameraFollow CameraF;
    public GameObject Playerss;
    public float SFood = 0.5f;
    public Text Scoress;
    public float FoodScore;
    public float Max;
    public float Min;
    public bool Maxi= false;
    public bool Mini = false;
    CameraFollow CFollow;

    // Use this for initialization
    void Start () {
        Players = (Player)FindObjectOfType(typeof(Player));
        CameraF = (CameraFollow)FindObjectOfType(typeof(CameraFollow));
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyUp(KeyCode.Equals))
        {
            
            if(Maxi == true)
            {
                Mini = false;
                return;
            }
            Debug.Log("Scaled Up");
            Players.transform.localScale += new Vector3(0.5f, 0.5f, 0.5f);
            Players.Speed =  Players.Speed * 0.8f;
            CameraF.ZoomIns();
        }
        if (Input.GetKeyUp(KeyCode.Minus))
        {
            if (Mini == true)
            {
                Maxi = false;
                return;
            }
            Debug.Log("Scaled Down");
            Players.transform.localScale -= new Vector3(0.5f, 0.5f, 0.5f);
            Players.Speed =  Players.Speed / 0.8f;
            CameraF.ZoomOuts();
        }
        if(Players.transform.localScale.x >= Max)
        {
            Players.transform.localScale = new Vector3(Max,Max,Max);
            Maxi = true;
        }
        if (Players.transform.localScale.x <= Min)
        {
            Players.transform.localScale = new Vector3(Min, Min, Min);
            Maxi = true;

        }
        SetCountText();
    }
    void SetCountText()
    {
        Scoress.text = "Eaten Food: " + FoodScore.ToString();
    }
}
