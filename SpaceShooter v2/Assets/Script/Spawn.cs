﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

    public float FoodSpawned = 0;
    public float StartingFood = 0;
    public float FoodMax = 10;
    public GameObject Foodss;
	// Use this for initialization
	void Start () {
        for (int o = 0; o < StartingFood; o++)
        {
            GameObject Stat;
            Stat = (GameObject)Instantiate(Foodss, new Vector3(Random.Range(12, -12), Random.Range(12, -12), this.transform.position.z), Quaternion.identity);
        }
        FoodSpawned = StartingFood;
        StartCoroutine(MyMethod());
	}
	
	// Update is called once per frame
    IEnumerator MyMethod()
    {
        while(true)
        {
            yield return new WaitForSeconds(1);
            if (FoodSpawned < FoodMax)
            {
                GameObject aste;
                aste = (GameObject)Instantiate(Foodss, new Vector3(Random.Range(12, -12), Random.Range(12, -12), this.transform.position.z), Quaternion.identity);
                FoodSpawned++;
            }

        }
    }
}
