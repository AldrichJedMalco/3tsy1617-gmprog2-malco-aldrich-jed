﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public GameObject Follow;
    public float ZoomOut = 10f;
    public float MaxSpeed;
    public float startValue = .5f;
    public float endValue = .5f;
    public Camera Cameras;
    private float StartCamera = 5;
    public float ZoomSpeed= 2;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 lookat = new Vector3(Follow.transform.position.x, Follow.transform.position.y, Follow.transform.position.z - ZoomOut);
        this.transform.position = Vector3.Lerp(this.transform.position, lookat, MaxSpeed * Time.deltaTime);

        Cameras.orthographicSize = Mathf.Lerp(Cameras.orthographicSize, StartCamera,ZoomSpeed * Time.deltaTime);
        
    }
    public void ZoomOuts()
    {
        StartCamera = Cameras.orthographicSize - .5f;

    }
    public void ZoomIns()
    {
        StartCamera=  Cameras.orthographicSize + 0.5f;
   
    }
    public void ZoomEat(float temp)
    {
        StartCamera = Cameras.orthographicSize + temp;

    }
}
