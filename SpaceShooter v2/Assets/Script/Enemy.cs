﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
    Eat,
    Run,
    Patrol,
}

public class Enemy : MonoBehaviour
{
    public float randomx;
    public float randomy;
    public float Speed = 0.5f;
    public State curState;
    public ESphereCo Eco;
    Player Players;
    EnemySpawn EnemiSpawn;

    void Update()
    {
        switch (curState)
        {
            case State.Run: UpdateRun(); break;
            case State.Eat: UpdateEat(); break;
            case State.Patrol: UpdatePatrol(); break;
        }
    }
    void Start()
    {
        Eco = (ESphereCo)FindObjectOfType(typeof(ESphereCo));
        Players = (Player)FindObjectOfType(typeof(Player));
        EnemiSpawn = (EnemySpawn)FindObjectOfType(typeof(EnemySpawn));
        curState = State.Patrol;
    }

    void UpdateEat()
    {
        if (Eco.TargetFollow != null)
        {
            if (Eco.TargetFollow.transform.localScale.magnitude < this.transform.localScale.magnitude)
            {
                this.transform.position = Vector2.MoveTowards(this.transform.position, Eco.TargetFollow.transform.position, Speed * Time.deltaTime);
                Debug.Log("Eat the bitch!");
            }
        }
        else if (Eco.nearByEnemy != null && Eco.TargetFollow == null)
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, Eco.nearByEnemy.transform.position, Speed * Time.deltaTime);
        }
        if (Eco.nearByEnemy == null && Eco.TargetFollow == null)
        {
            curState = State.Patrol;
        }

    }

    void UpdateRun()
    {
        if (Eco.EnemyTarget.transform.localScale.magnitude > this.transform.localScale.magnitude)
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, Eco.EnemyTarget.transform.position, -1 * Speed * Time.deltaTime);
            Debug.Log("RAN AWAY!");
        }
        if (Vector3.Distance(Eco.EnemyTarget.transform.position, transform.position) >= 4)
        {
            curState = State.Patrol;
        }
        else if (Eco.EnemyTarget.transform.localScale.magnitude < this.transform.localScale.magnitude)
        {
            curState = State.Eat;
        }
    }

    void UpdatePatrol()
    {
        Vector3 target = new Vector3(randomx, randomy,0);
        if (target == this.transform.position)
        {
            randomy = Random.Range(-12f, 12f);
            randomx = Random.Range(-12f, 12f);
        }
        this.transform.position = Vector2.MoveTowards(this.transform.position, target, Speed * Time.deltaTime);

        if (Eco.nearByEnemy != null)
        {
            curState = State.Eat;
        }
        if (Eco.TargetFollow.transform.localScale.magnitude > this.transform.localScale.magnitude)
        {
            curState = State.Run;
        }
    }
    void OnCollisionEnter(Collision sometin)
    {
        if (sometin.collider.gameObject.tag == "Food")
        {
            Destroy(sometin.gameObject);
            this.transform.localScale += new Vector3(0.05f, 0.05f, 0.05f);
            Speed = Speed * 0.95f;
            Eco.nearByEnemy = null;
        }
        if (sometin.collider.gameObject.tag == "lol" || sometin.collider.gameObject.tag == "Enemy")
        {
            if (Eco.EnemyTarget.transform.localScale.magnitude < this.transform.localScale.magnitude)
            {
                Destroy(sometin.gameObject);
                EnemiSpawn.EnemySpawned--;
            } else Destroy(this.gameObject);
        }
    }

}

