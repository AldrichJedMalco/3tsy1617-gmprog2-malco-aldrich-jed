﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawning : MonoBehaviour
{

    public GameObject[] Aster;
    public float Speeds = 0.5f;
    public float StartSpawn = 0.1f;
    public float IntervalSpawn = 1f;
    private int Selected;
    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Spawns", StartSpawn, IntervalSpawn);
    }

    // Update is called once per frame
    void Update()
    {
    }
    void Spawns()
    {
        int spawn = Random.Range(7, -7);
        Vector3 posit = new Vector3(spawn, this.transform.position.y, this.transform.position.z);
        Selected = Random.Range(0, Aster.Length);
        GameObject aste;
        aste = (GameObject)Instantiate(Aster[Selected], new Vector3(Random.Range(7, -7), this.transform.position.y, this.transform.position.z), Quaternion.identity);
        Destroy(aste, 4);
    }
}
