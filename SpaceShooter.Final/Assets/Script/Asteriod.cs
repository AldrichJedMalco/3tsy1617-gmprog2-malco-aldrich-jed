﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteriod : MonoBehaviour {

    public float Speed = 5f;
    PLAYERm Playerss;
	// Use this for initialization
	void Start () {
        Playerss = (PLAYERm)FindObjectOfType(typeof(PLAYERm));
    }
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.down * Speed * Time.deltaTime);
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
            Playerss.Lives--;
        }
    }

}
