﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Start2End : MonoBehaviour {
    public Image Starts;
    public Image Ends;
    PLAYERm Playerss;
	// Use this for initialization
	void Start () {
        Ends.enabled = false;
        PauseGame();
        Playerss = (PLAYERm)FindObjectOfType(typeof(PLAYERm));
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKey)
        {
            Starts.enabled = false;
            ContinueGame();
        }
        LifeCheck();
	}
    private void PauseGame()
    {
        Time.timeScale = 0;
    }
    private void ContinueGame()
    {
        Time.timeScale = 1;
    }
    public void LifeCheck()
    {
        if (Playerss.Lives <= 0)
        {
            Ends.enabled = true;
            PauseGame();
        }
    }
}
