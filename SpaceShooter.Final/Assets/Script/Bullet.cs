﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float Velocity = 5f;
    PLAYERm Playerss;
	// Use this for initialization
	void Start () {
        Playerss = (PLAYERm)FindObjectOfType(typeof(PLAYERm));
    }
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.up * Velocity * Time.deltaTime);
        Destroy(this.gameObject, 3);
	}
    void OnTriggerEnter(Collider Enemy)
    {
        if (Enemy.gameObject.tag == "Aster")
        {
            Destroy(Enemy.gameObject);
            Playerss.Scores += 2;
        }
    }
}
