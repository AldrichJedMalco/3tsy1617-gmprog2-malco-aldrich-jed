﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PLAYERm : MonoBehaviour {
    public float Speeds = 0.5f;
    public bool Rotatte = false;
    public GameObject Bullet;
    public float Lives = 3f;
    public float Scores;
    
    GameObject BulletClone;
    public Text LivessText;
    public Text ScoreText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * Speeds * Time.deltaTime);
            if (transform.position.x < -7f)
            {
                transform.Translate(0.2f, 0, 0);
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * Speeds * Time.deltaTime);
            if (transform.position.x > 7f)
            {
                transform.Translate(-0.2f, 0, 0);
            }
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.up * Speeds * Time.deltaTime);
            if (transform.position.y > 5.7f)
            {
                transform.Translate(0, -0.2f, 0);
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.down * Speeds * Time.deltaTime);
            if (transform.position.y < -4f)
            {
                transform.Translate(0, 0.2f, 0);
            }
        }
        if (Input.GetKey(KeyCode.Space))
        {
            Vector3 offset = new Vector3(this.transform.position.x, this.transform.position.y + 1f, this.transform.position.z);
            BulletClone = (GameObject)Instantiate(Bullet, offset, Quaternion.identity);
        }
        SetLivesLeft();
        SetScoreLeft();
    }
    void SetLivesLeft()
    {
        LivessText.text = "LIVES: " + Lives;
    }
    void SetScoreLeft()
    {
        ScoreText.text = "Score: " + Scores;
    }
}
