﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour
{
    public float BackGroundSpeed = 0.5f;
    // Use this for initialization
    // Update is called once per frame
    void Update()
    {
        MeshRenderer mr = GetComponent<MeshRenderer>();
        Material mt = mr.material;
        Vector3 offset = mt.mainTextureOffset;
        offset.y += BackGroundSpeed * Time.deltaTime;
        mt.mainTextureOffset = offset;

    }
}